/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <stddef.h>

#include <fountain/error.h>

#include "error.h"

void
fountain_error_init(fountain_error_t * error)
{
    error->err = FOUNTAIN_ERROR_NONE;
}

void
error_fread(fountain_error_t * error, FILE * file)
{
    if (error != NULL) {
        error->err = FOUNTAIN_ERROR_FREAD;
        error->data.fread.file = file;
    }
}

void
error_system(fountain_error_t * error, const char * call)
{
    if (error != NULL) {
        error->err = FOUNTAIN_ERROR_SYSTEM;
        error->data.system.err = errno;
        error->data.system.call = call;
    }
}
