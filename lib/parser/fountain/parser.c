/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fountain/error.h>
#include <fountain/data/screenplay.h>
#include <fountain/parser/parser.h>
#include <fountain/parser/fountain/parser.h>

#include "error.h"

#define CHUNK_SIZE 32

typedef struct line_s line_t;
typedef struct reader_s reader_t;
typedef struct parser_s parser_t;

struct line_s
{
    reader_t * reader;
    unsigned siz;
    char * buf;
    line_t * next;
};

struct reader_s
{
    FILE * file;

    /* Linked list of unused lines. */
    line_t * unused_lines;

    /* Linked list of unread lines. */
    line_t * unread_lines;
};

struct parser_s
{
    reader_t reader;
    const fountain_data_config_t * config;
    fountain_error_t * error;
};

static int
line_init(reader_t * reader, line_t * line, fountain_error_t * error)
{
    line->reader = reader;
    line->siz = CHUNK_SIZE;
    line->buf = malloc(CHUNK_SIZE);
    if (line->buf == NULL) {
        error_system(error, "malloc");
    }
    return line->buf == NULL;
}

static void
line_deinit(line_t * line)
{
    free(line->buf);
}

static line_t *
line_new(reader_t * reader, fountain_error_t * error)
{
    line_t * line;
    int ret;

    line = malloc(sizeof(*line));
    if (line == NULL) {
        error_system(error, "malloc");
        return NULL;
    }

    ret = line_init(reader, line, error);
    if (ret) {
        free(line);
        return NULL;
    }

    return line;
}

static void
line_destroy(line_t * line)
{
    line_deinit(line);
    free(line);
}

static int
line_alloc_chunk(line_t * line, fountain_error_t * error)
{
    unsigned newsiz = line->siz + CHUNK_SIZE;
    char * new = realloc(line->buf, newsiz);
    if (new == NULL) {
        error_system(error, "realloc");
        return 1;
    }

    line->siz = newsiz;
    line->buf = new;

    return 0;
}

static void
line_unuse(line_t * line)
{
    reader_t * reader = line->reader;
    line->next = reader->unused_lines;
    reader->unused_lines = line;
}

static int
line_is_empty(const line_t * line)
{
    return line->buf[0] == '\0';
}

static void
reader_init(reader_t * reader, FILE * file)
{
    reader->file = file;
    reader->unused_lines = NULL;
    reader->unread_lines = NULL;
}

static void
reader_deinit(reader_t * reader)
{
    line_t * line = reader->unused_lines;
    line_t * next;
    while (line != NULL) {
        next = line->next;
        line_destroy(line);
        line = next;
    }
}

static line_t *
reader_get_line(reader_t * reader, fountain_error_t * error)
{
    line_t * line = reader->unused_lines;

    /* Reuse an unused line if possible. */
    if (line != NULL) {
        reader->unused_lines = line->next;
        return line;
    }

    return line_new(reader, error);
}

/**
 * Read a line.
 *
 * @param reader the reader
 * @param linep a pointer to the line (pointer) to fill in
 * @param error the error to fill in in case of an error
 * @return 0 on success, -1 on an error, or 1 on EOF
 */
static int
reader_read_line(reader_t * reader, line_t ** linep,
        fountain_error_t * error)
{
    line_t * line;
    char * buf;
    char * end;
    int c;
    int ret;

    line = reader->unread_lines;
    if (line != NULL) {
        reader->unread_lines = line->next;
        *linep = line;
        return 0;
    }

    line = reader_get_line(reader, error);
    if (line == NULL) {
        return -1;
    }
    *linep = line;

    buf = line->buf;
    end = buf + line->siz;

    c = fgetc(reader->file);
    if (c == EOF) {
        if (ferror(reader->file)) {
            goto error_ferror;
        } else {
            line_unuse(line);
            return 1;
        }
    } else if (c == '\n') {
        *buf = '\0';
        return 0;
    }
    *buf = (char)c;
    buf++;

    for (;;) {
        for (; buf < end; ++buf) {
            c = fgetc(reader->file);
            if (c == EOF && ferror(reader->file)) {
                goto error_ferror;
            } else if (c == EOF || c == '\n') {
                *buf = '\0';
                return 0;
            }

            *buf = (char)c;
        }

        char * old = line->buf;
        ret = line_alloc_chunk(line, error);
        if (ret) {
            line_unuse(line);
            return -1;
        }
        buf += line->buf - old;
        end += line->buf - old;
        end += CHUNK_SIZE;
    }

error_ferror:
    __attribute__((cold));
    error_fread(error, reader->file);
    line_unuse(line);
    return -1;
}

static void
reader_unread_line(reader_t * reader, line_t * line)
{
    line->next = reader->unread_lines;
    reader->unread_lines = line;
}

static void
parser_init(parser_t * parser, FILE * file,
        const fountain_data_config_t * config, fountain_error_t * error)
{
    reader_init(&parser->reader, file);
    parser->config = config;
    parser->error = error;
}

static void
parser_deinit(parser_t * parser)
{
    reader_deinit(&parser->reader);
}

static inline int
parser_read_line(parser_t * parser, line_t ** linep)
{
    return reader_read_line(&parser->reader, linep, parser->error);
}

static inline void
parser_unread_line(parser_t * parser, line_t * line)
{
    reader_unread_line(&parser->reader, line);
}

static inline fountain_screenplay_t *
parser_screenplay_new(parser_t * parser)
{
    return fountain_screenplay_new(parser->config, parser->error);
}

static char * __attribute__((pure))
ignore_leading_spaces(const char * str)
{
    const char * s;
    for (s = str; isspace(*s); ++s) {
        /* empty */
    }
    return (char *)s;
}

static int
parse_title_page_key(parser_t * parser,
        fountain_screenplay_t * screenplay,
        line_t * lines)
{
    fountain_title_page_t * title_page;
    line_t * line = lines;
    line_t * next;
    const char * key;
    char * colon;
    char * value;
    int ret;

    /* There is a colon on the first line. */
    colon = strchr(line->buf, ':');
    *colon = '\0';
    key = line->buf;

    value = ignore_leading_spaces(colon + 1);
    printf("key: %s\n", line->buf);
    printf("value: %s\n", value);

    for (line = line->next; line != NULL; line = line->next) {
        value = ignore_leading_spaces(line->buf);
        printf("value: %s\n", value);
    }

    title_page = fountain_screenplay_title_page(screenplay);
    ret = fountain_title_page_add_entry(title_page, key, NULL,
            parser->error);

    /* Unuse all lines. */
    line = lines;
    while (line != NULL) {
        next = line->next;
        line_unuse(line);
        line = next;
    }

    return ret;
}

static int
parse_title_page(parser_t * parser, fountain_screenplay_t * screenplay)
{
    line_t * line;
    int ret;

    /*
     * Parse the key-value pairs by first collecting the linespan of
     * each key in this loop and then doing the actual parsing in the
     * subroutine.
     */
    ret = parser_read_line(parser, &line);
    for (;;) {
        line_t * first = line;
        line_t * last = line;

        if (ret < 0) {
            /* error */
            return ret;
        } else if (ret > 0) {
            /* eof */
            return 0;
        }

        if (strchr(line->buf, ':') == NULL) {
            /* no key */
            parser_unread_line(parser, line);
            return 0;
        }

        /*
         * Collect indented value lines. From the syntax specification:
         *
         * "Indenting is 3 or more spaces, or a tab."
         *
         * Note that last value of line (that fails to be an indented
         * value line) is acted upon in the next iteration.
         */
        while ((ret = parser_read_line(parser, &line)) == 0
                && (line->buf[0] == '\t'
                    || (line->buf[0] == ' '
                        && line->buf[1] == ' '
                        && line->buf[2] == ' ')))
        {
            last->next = line;
            last = line;
        }
        last->next = NULL;

        /* The subroutine unuses the lines. */
        if (parse_title_page_key(parser, screenplay, first)) {
            return 1;
        }
    }
}

static fountain_screenplay_t *
parse_screenplay(parser_t * parser)
{
    fountain_screenplay_t * screenplay;
    line_t * line;
    int ret;

    screenplay = parser_screenplay_new(parser);
    if (screenplay == NULL) {
        return NULL;
    }

    ret = parse_title_page(parser, screenplay);
    if (ret < 0) {
        goto error;
    }

    while ((ret = parser_read_line(parser, &line)) == 0) {
        /* TODO: parse */
        printf("%s\n", line->buf);
        line_unuse(line);
    }
    if (ret < 0) {
        goto error;
    }

    return screenplay;
error:
    fountain_screenplay_destroy(screenplay);
    return NULL;
}

static fountain_screenplay_t *
parse_fountain(FILE * file, const fountain_data_config_t * config,
        fountain_error_t * error)
{
    parser_t parser;
    fountain_screenplay_t * screenplay;
    parser_init(&parser, file, config, error);
    screenplay = parse_screenplay(&parser);
    parser_deinit(&parser);
    return screenplay;
}

const fountain_parser_info_t fountain_fountain_parser_info = {
    &parse_fountain
};
