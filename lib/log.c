/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <stdlib.h>

#include <fountain/log.h>

#include "log.h"

void
fountain_log_init(fountain_log_t * log, fountain_log_func_t * func,
    void * ptr)
{
    log->func = func;
    log->ptr = ptr;
}

void
fountain_log_deinit(fountain_log_t * log)
{
    // Empty.
}

fountain_log_t *
fountain_log_new(fountain_log_func_t * func, void * ptr)
{
    fountain_log_t * log = malloc(sizeof(*log));
    if (log != NULL) {
        fountain_log_init(log, func, ptr);
    }
    return log;
}

void
fountain_log_destroy(fountain_log_t * log)
{
    fountain_log_deinit(log);
    free(log);
}
