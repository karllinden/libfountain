/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef SECTION_H
# define SECTION_H

# include <fountain/data/data-config.h>
# include <fountain/data/section.h>

# include "dllist.h"

struct section_list_s
{
    dllist_t list;
};
typedef struct section_list_s section_list_t;

struct fountain_section_s
{
    const fountain_data_config_t * config;

    /*
     * Sections can have subsections, which can have subsubsections, and
     * so on.
     */
    section_list_t sections;

    /* TODO: text */

    char data[];
};

void section_init(
    fountain_section_t * section,
    const fountain_data_config_t * config);

void section_deinit(fountain_section_t * section);

void section_list_init(
    section_list_t * list,
    const fountain_data_config_t * config);

void section_list_deinit(section_list_t * list);

fountain_section_t * section_list_last(const section_list_t * list);

fountain_section_t * section_list_append(
    section_list_t * list,
    const fountain_data_config_t * config,
    fountain_error_t * error);

#endif /* SECTION_H */
