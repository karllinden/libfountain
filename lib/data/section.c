/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <fountain/data/data-config.h>
#include <fountain/data/section.h>

#include "section.h"

static void
section_deinit_voidp(void * section)
{
    section_deinit(section);
}

void
section_init(fountain_section_t * section,
    const fountain_data_config_t * config)
{
    section->config = config;
    section_list_init(&section->sections, config);

    if (config->section_init != NULL) {
        (*config->section_init)(section, config->pointer);
    }
}

void
section_deinit(fountain_section_t * section)
{
    section_list_deinit(&section->sections);

    const fountain_data_config_t * config = section->config;
    if (config->section_deinit != NULL) {
        (*config->section_deinit)(section, config->pointer);
    }
}

void
section_list_init(section_list_t * list,
    const fountain_data_config_t * config)
{
    dllist_init(&list->list,
        sizeof(fountain_section_t) + config->section_size);
}

void
section_list_deinit(section_list_t * list)
{
    dllist_deinit(&list->list, section_deinit_voidp);
}

fountain_section_t *
section_list_last(const section_list_t * list)
{
    dllist_elem_t * elem = dllist_last(&list->list);
    return elem != NULL ? dllist_elem_data(elem) : NULL;
}

fountain_section_t *
section_list_append(section_list_t * list,
    const fountain_data_config_t * config, fountain_error_t * error)
{
    dllist_elem_t * elem = dllist_elem_new(&list->list, error);
    if (elem == NULL) {
        return NULL;
    }

    fountain_section_t * section = dllist_elem_data(elem);
    section_init(section, config);

    dllist_append(&list->list, elem);

    return section;
}

void *
fountain_section_data(fountain_section_t * section)
{
    return section->data;
}

fountain_section_t *
fountain_section_last(const fountain_section_t * section)
{
    return section_list_last(&section->sections);
}

fountain_section_t *
fountain_section_append_section(fountain_section_t * section,
    fountain_error_t * error)
{
    return section_list_append(&section->sections, section->config,
        error);
}
