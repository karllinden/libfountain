/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fountain/fountain.h>

#include "error.h"

/* TODO: use these */
#define FOUNTAIN_TEXT_BOLD      0x1
#define FOUNTAIN_TEXT_ITALICS   0x2
#define FOUNTAIN_TEXT_UNDERLINE 0x4

struct formatting_s
{
    /*
     * The position where the formatting starts to apply.
     */
    uint16_t pos;

    /*
     * The flags to apply.
     */
    uint16_t flags;
};
typedef struct formatting_s formatting_t;

struct fountain_text_s
{
    /*
     * The unformatted text. NUL-terminated.
     */
    char * text;

    /*
     * A buffer holding both the formatting array (array of formatting_t
     * and the text. The formatting array comes first so casting buf to
     * formatting_t yields the formatting array. The formatting array
     * ends at text, so that is the end.
     */
    void * buf;

    const fountain_data_config_t * config;
    char data[];
};

struct formspec_s
{
    const char * str;
    uint16_t flag;
};
typedef struct formspec_s formspec_t;

struct segment_s
{
    char * ptr;
    uint16_t add_flags;
    uint16_t del_flags;
};
typedef struct segment_s segment_t;

struct segments_s
{
    segment_t * segments;
    size_t size;
};
typedef struct segments_s segments_t;

#define N_ALL_FORMSPECS 3

static const formspec_t all_formspecs[N_ALL_FORMSPECS] = {
    {"**", FOUNTAIN_TEXT_BOLD},
    {"*", FOUNTAIN_TEXT_ITALICS},
    {"_", FOUNTAIN_TEXT_UNDERLINE}
};

static void
segments_init(segments_t * segments)
{
    segments->segments = NULL;
    segments->size = 0;
}

static void
segments_deinit(segments_t * segments)
{
    free(segments->segments);
}

#define STRSPLIT_CHUNK_SIZE 8
static char **
strsplit(char * str, char delim, size_t * sizep,
        fountain_error_t * error)
{
    size_t size = 0;
    size_t len = 0;
    char ** arr = NULL;

    while (str != NULL) {
        size += STRSPLIT_CHUNK_SIZE;
        char ** newarr = realloc(arr, size * sizeof(*arr));
        if (newarr == NULL) {
            free(arr);
            error_system(error, "realloc");
            return NULL;
        }

        for (; len < size && str != NULL; ++len) {
            char * d = strchr(str, delim);
            if (d != NULL) {
                *d = '\0';
                d++;
            }

            arr[len] = str;
            str = d;
        }
    }

    if (len < size) {
        size = len;
        char ** newarr = realloc(arr, size * sizeof(*arr));
        if (newarr == NULL) {
            free(arr);
            error_system(error, "realloc");
            return NULL;
        }
    }

    *sizep = size;
    return arr;
}

static int
line_to_segments(char * line, segments_t * segments)
{
    printf("line: %s\n", line);
    /* TODO */
    return 1;
}

static fountain_text_t *
text_from_copy(char * raw, const fountain_data_config_t * config,
        fountain_error_t * error)
{
    fountain_text_t * result = NULL;
    segments_t * segmentss = NULL;
    size_t n_lines = 0;
    char ** lines = strsplit(raw, '\n', &n_lines, error);
    if (lines == NULL) {
        goto end;
    }

    segmentss = malloc(n_lines * sizeof(*segmentss));
    if (segmentss == NULL) {
        error_system(error, "malloc");
        goto end;
    }
    for (size_t i = 0; i < n_lines; ++i) {
        segments_init(segmentss + i);
    }

    int fail = 0;
    for (size_t i = 0; i < n_lines && !fail; ++i) {
        fail = line_to_segments(lines[i], segmentss + i);
    }
    if (fail) {
        goto end;
    }

    /* TODO: convert the segments to fountain_text_t */

end:
    if (segmentss != NULL) {
        for (size_t i = 0; i < n_lines; ++i) {
            segments_deinit(segmentss + i);
        }
    }
    free(raw);
    return result;
}

fountain_text_t *
fountain_text_from_raw(const char * raw,
        const fountain_data_config_t * config,
        fountain_error_t * error)
{
    char * copy = strdup(raw);
    if (copy == NULL) {
        error_system(error, "strdup");
        return NULL;
    }
    return text_from_copy(copy, config, error);
}

void
fountain_text_destroy(fountain_text_t * text)
{
    if (text == NULL) {
        return;
    }

    const fountain_data_config_t * config = text->config;
    if (config->text_deinit != NULL) {
        (*config->text_deinit)(text, config->pointer);
    }

    free(text->buf);
    free(text);
}
