/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>

#include <fountain/data/text.h>
#include <fountain/data/title-page.h>

#include "error.h"
#include "title-page.h"

static uint16_t
to_index(uint32_t rindex)
{
    return (uint16_t)rindex;
}

static int
has_found_key(uint32_t rindex)
{
    return (rindex & 0xFFFF0000) != 0;
}

static uint32_t
to_rindex(uint16_t index, int found_key)
{
    uint32_t ret = (uint16_t)index;
    if (found_key) {
        ret |= 0xFFFF0000;
    }
    return ret;
}

/**
 * Get the insertion index of the given key.
 *
 * @param title_page the title page
 * @param key the key
 * @return the insertion index, or the index of the current
 */
static uint32_t __attribute__((pure))
get_rindex(const fountain_title_page_t * title_page, const char * key)
{
    title_page_entry_t * entries = title_page->entries;
    int cmp = 0;
    uint16_t min = 0; /* inclusive */
    uint16_t max = title_page->size; /* exclusive */

    while (min < max) {
        uint16_t idx = min + (max - min) / 2;
        cmp = strcmp(key, entries[idx].key);
        if (cmp < 0) {
            max = idx;
        } else if (cmp > 0) {
            min = idx + 1;
        } else {
            return to_rindex(idx, 1);
        }
    }

    return to_rindex(min, 0);
}

void
title_page_init(fountain_title_page_t * title_page)
{
    title_page->size = 0;
    title_page->entries = NULL;
}

void
title_page_deinit(fountain_title_page_t * title_page)
{
    title_page_entry_t * entry = title_page->entries;
    title_page_entry_t * end = entry + title_page->size;
    while (entry < end) {
        puts(entry->key);
        free(entry->key);
        fountain_text_destroy(entry->value);
        entry++;
    }
    free(title_page->entries);
}

int
fountain_title_page_add_entry(fountain_title_page_t * title_page,
        const char * key,
        fountain_text_t * value,
        fountain_error_t * error)
{
    char * keycopy;
    title_page_entry_t * entry;
    uint32_t rindex;
    uint16_t i;

    /*
     * Copy key first so that the array does not have to be restored in
     * case allocation fails.
     */
    keycopy = strdup(key);
    if (keycopy == NULL) {
        error_system(error, "strdup");
        return 1;
    }

    rindex = get_rindex(title_page, key);
    i = to_index(rindex);

    if (has_found_key(rindex)) {
        entry = title_page->entries + i;
        free(entry->key);
        fountain_text_destroy(entry->value);
    } else {
        uint16_t newsize = title_page->size + 1;
        title_page_entry_t * new =
            realloc(title_page->entries, newsize * sizeof(*new));
        if (new == NULL) {
            error_system(error, "realloc");
            return 1;
        }

        memmove(new + i + 1, new + i,
                (title_page->size - i) * sizeof(*new));
        entry = new + i;

        title_page->size = newsize;
        title_page->entries = new;
    }

    entry->key = keycopy;
    entry->value = value;

    return 0;
}
