/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <stdlib.h>

#include "dllist.h"
#include "error.h"

/*
 * TODO: Allocate a chunk of elements at once to avoid calls to malloc.
 */

void
dllist_init(dllist_t * list, size_t data_size)
{
    list->first     = NULL;
    list->last      = NULL;
    list->data_size = data_size;
}

void
dllist_deinit(dllist_t * list, void (*data_destroy_func)(void *))
{
    dllist_elem_t * elem = list->first;
    dllist_elem_t * next;
    while (elem != NULL) {
        if (data_destroy_func != NULL) {
            (*data_destroy_func)(dllist_elem_data(elem));
        }
        next = dllist_elem_next(elem);
        dllist_elem_destroy(elem);
        elem = next;
    }
}

dllist_elem_t *
dllist_elem_new(dllist_t * list, fountain_error_t * error)
{
    dllist_elem_t * elem = malloc(sizeof(*elem) + list->data_size);
    if (elem == NULL) {
        error_system(error, "malloc");
    }
    return elem;
}

void
dllist_elem_destroy(dllist_elem_t * elem)
{
    free(elem);
}

dllist_elem_t *
dllist_elem_next(dllist_elem_t * elem)
{
    return elem->next;
}

void *
dllist_elem_data(dllist_elem_t * elem)
{
    return elem->data;
}

void
dllist_append(dllist_t * list, dllist_elem_t * elem)
{
    dllist_elem_t * last = list->last;
    if (last != NULL) {
        last->next = elem;
    } else {
        list->first = elem;
    }
    elem->prev = last;
    elem->next = NULL;
    list->last = elem;
}

void
dllist_prepend(dllist_t * list, dllist_elem_t * elem)
{
    dllist_elem_t * first = list->first;
    if (first != NULL) {
        first->prev = elem;
    } else {
        list->last = elem;
    }
    elem->prev = NULL;
    elem->next = first;
    list->first = elem;
}

dllist_elem_t *
dllist_first(const dllist_t * list)
{
    return list->first;
}

dllist_elem_t *
dllist_last(const dllist_t * list)
{
    return list->last;
}
