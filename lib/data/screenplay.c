/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>

#include <fountain/fountain.h>

#include "dllist.h"
#include "error.h"
#include "section.h"
#include "title-page.h"

struct fountain_screenplay_s
{
    /*
     * A copy of the configuration structure.
     */
    fountain_data_config_t config;

    /*
     * The title page.
     */
    fountain_title_page_t title_page;

    /*
     * A list of sections (of type fountain_section_t).
     */
    section_list_t sections;

    /*
     * User defined data. Used for extending the functionality of the
     * data model.
     */
    char data[];
};

/*
 * Configuration to use if a NULL configuration is given.
 */
static const fountain_data_config_t no_config = {
    NULL,
    0,
    NULL,
    NULL,
    0,
    NULL,
    NULL,
    0,
    NULL,
    NULL
};

static void
screenplay_init(fountain_screenplay_t * screenplay,
    const fountain_data_config_t * config)
{
    memcpy(&screenplay->config, config, sizeof(screenplay->config));
    title_page_init(&screenplay->title_page);
    section_list_init(&screenplay->sections, config);

    if (config->screenplay_init != NULL) {
        (*config->screenplay_init)(screenplay, config->pointer);
    }
}

static void
screenplay_deinit(fountain_screenplay_t * screenplay)
{
    title_page_deinit(&screenplay->title_page);
    section_list_deinit(&screenplay->sections);

    const fountain_data_config_t * config = &screenplay->config;
    if (config->screenplay_deinit != NULL) {
        (*config->screenplay_deinit)(screenplay, config->pointer);
    }
}

fountain_screenplay_t *
fountain_screenplay_new(const fountain_data_config_t * config,
    fountain_error_t * error)
{
    const fountain_data_config_t * actual_config =
        config != NULL ? config : &no_config;

    fountain_screenplay_t * screenplay = malloc(
        sizeof(*screenplay) + actual_config->screenplay_size);
    if (screenplay == NULL) {
        error_system(error, "malloc");
        return NULL;
    }

    screenplay_init(screenplay, actual_config);

    return screenplay;
}

void
fountain_screenplay_destroy(fountain_screenplay_t * screenplay)
{
    if (screenplay != NULL) {
        screenplay_deinit(screenplay);
        free(screenplay);
    }
}

void *
fountain_screenplay_data(fountain_screenplay_t * screenplay)
{
    return screenplay->data;
}

fountain_title_page_t *
fountain_screenplay_title_page(fountain_screenplay_t * screenplay)
{
    return &screenplay->title_page;
}

fountain_section_t *
fountain_screenplay_last_section(
    const fountain_screenplay_t * screenplay)
{
    return section_list_last(&screenplay->sections);
}

fountain_section_t *
fountain_screenplay_append_section(fountain_screenplay_t * screenplay,
    fountain_error_t * error)
{
    return section_list_append(&screenplay->sections,
        &screenplay->config, error);
}
