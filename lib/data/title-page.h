/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef DATA_TITLE_PAGE_H
# define DATA_TITLE_PAGE_H

# include <stdint.h>

# include <fountain/data/text.h>
# include <fountain/data/title-page.h>

struct title_page_entry_s
{
    char * key;
    fountain_text_t * value;
};
typedef struct title_page_entry_s title_page_entry_t;

struct fountain_title_page_s
{
    uint16_t size;
    title_page_entry_t * entries;
};

void title_page_init(fountain_title_page_t * title_page);
void title_page_deinit(fountain_title_page_t * title_page);

#endif /* _FOUNTAIN_DATA_TITLE_PAGE_H_ */
