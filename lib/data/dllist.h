/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef DLLIST_H
# define DLLIST_H

# include <stddef.h>

# include <fountain/error.h>

struct dllist_elem_s
{
    struct dllist_elem_s * prev;
    struct dllist_elem_s * next;
    char                   data[];
};
typedef struct dllist_elem_s dllist_elem_t;

struct dllist_s
{
    dllist_elem_t * first;
    dllist_elem_t * last;
    size_t          data_size;
};
typedef struct dllist_s dllist_t;

/**
 * Initialize a doubly linked list.
 *
 * @param list the list to initialize
 */
void dllist_init(dllist_t * list, size_t data_size);

/**
 * Deinitialize a list.
 *
 * @param list the list to deinitialize
 */
void dllist_deinit(dllist_t * list, void (*data_destroy_func)(void *));

/**
 * Create a new list element.
 *
 * @param data_size the size of the list element data
 * @param error the error
 * @return the newly created list element, or NULL if memory could not
 *         be allocated
 */
dllist_elem_t * dllist_elem_new(
    dllist_t * list,
    fountain_error_t * error);

/**
 * Destroy a list element.
 *
 * This function does not destroy the list element data.
 *
 * @param elem the list element
 */
void dllist_elem_destroy(dllist_elem_t * elem);

/**
 * Get the next element after a given list element.
 *
 * @param elem the list element
 * @return the next list element
 */
dllist_elem_t * dllist_elem_next(dllist_elem_t * elem);

/**
 * Get the data of a list element.
 *
 * @param elem the list element
 * @return the data
 */
void * dllist_elem_data(dllist_elem_t * elem);

/**
 * Append a list element to a list.
 *
 * @param list the list to append to
 * @param elem the list element to append
 */
void dllist_append(dllist_t * list, dllist_elem_t * elem);

/**
 * Prepend a list element to a list.
 *
 * @param list the list to prepend to
 * @param elem the list element to prepend
 */
void dllist_prepend(dllist_t * list, dllist_elem_t * elem);

/**
 * Get the first element of a list.
 *
 * @param list the list
 * @return the first element, or NULL if the list is empty
 */
dllist_elem_t * dllist_first(const dllist_t * list);

/**
 * Get the last element of a list.
 *
 * @param list the list
 * @return the last element, or NULL if the list is empty
 */
dllist_elem_t * dllist_last(const dllist_t * list);

#endif /* DLLIST_H */
