/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>

#include <fountain/parse.h>
#include <fountain/parser/fountain/parser.h>

fountain_screenplay_t *
fountain_parse(const fountain_parser_info_t * info, FILE * file,
    const fountain_data_config_t * config, fountain_error_t * error)
{
    return (*info->parse)(file, config, error);
}

fountain_screenplay_t *
fountain_parse_fountain(FILE * file,
    const fountain_data_config_t * config, fountain_error_t * error)
{
    return fountain_parse(
            &fountain_fountain_parser_info,
            file,
            config,
            error);
}

