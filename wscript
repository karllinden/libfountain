#!/usr/bin/env python
# encoding: utf-8
#
# This file is part of libfountain.
#
# Copyright (C) 2018 Karl Linden
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

from __future__ import print_function

import os

VERSION = '0.0.1'
APPNAME = 'libfountain'

extra_cflags = [
    '-fipa-pure-const',
    '-fstrict-overflow'
]
warning_cflags = [
    '-Wall',
    '-Winline',
    '-Wmissing-declarations',
    '-Wshadow',
    '-Wsign-compare',
    '-Wstrict-overflow=5',
    '-Wsuggest-attribute=const',
    '-Wsuggest-attribute=pure',
    '-Wtype-limits',
    '-pedantic'
]

def options(opt):
    opt.load('compiler_c')
    opt.load('gnu_dirs')

def configure(conf):
    conf.load('compiler_c')
    conf.load('gccdeps')
    conf.load('gnu_dirs')

    conf.env.append_unique('CFLAGS', '-std=gnu99')
    conf.env.append_unique('CFLAGS', extra_cflags)
    conf.env.append_unique('CFLAGS', warning_cflags)

    conf.env['PACKAGE_NAME'] = APPNAME
    conf.env['PACKAGE_VERSION'] = conf.env['VERSION'] = VERSION
    conf.env['PACKAGE_URL'] = \
            'https://gitlab.com/karllinden/libfountain'
    conf.env['PACKAGE_BUGREPORT'] = conf.env['PACKAGE_URL'] + '/issues'
    conf.env['SLOT'] = VERSION.split('.')[0]
    conf.env['PACKAGE'] = APPNAME + '-' + conf.env['SLOT']

    conf.env['PKGCONFIGDIR'] = os.path.join(
            conf.env['LIBDIR'],
            'pkgconfig')
    conf.env['SLOTINCLUDEDIR'] = os.path.join(
            conf.env['INCLUDEDIR'],
            conf.env['PACKAGE'])

    install_dirs = [
        ('PREFIX',      'Install prefix'),
        ('EXEC_PREFIX', 'Installation prefix for binaries'),
        ('LIBDIR',      'Library directory'),
        ('INCLUDEDIR',  'Header directory'),
    ]

    print()
    print('Install directories')
    for name,descr in install_dirs:
        conf.msg(descr, conf.env[name], color='CYAN')

    conf.write_config_header('config.h')

def build(bld):
    bld.env.append_unique('CFLAGS', '-DHAVE_CONFIG_H=1')
    bld.env['INCLUDES'] = [bld.bldnode.abspath()]

    bld.env['INCLUDES_LIBFOUNTAIN'] = [bld.srcnode.find_dir('./include')]
    bld.env['LIBPATH_LIBFOUNTAIN'] = [bld.bldnode]
    lib_target = 'fountain-' + bld.env['SLOT']
    bld.env['LIB_LIBFOUNTAIN'] = [lib_target]

    sources = [
        'lib/error.c',
        'lib/log.c',
        'lib/parse.c',
        'lib/data/dllist.c',
        'lib/data/screenplay.c',
        'lib/data/section.c',
        'lib/data/title-page.c',
        'lib/data/text.c',
        'lib/parser/fountain/parser.c'
    ]

    bld(
        features     = 'c cshlib',
        target       = lib_target,
        source       = sources,
        includes     = bld.env['INCLUDES_LIBFOUNTAIN'] + ['lib'],
        install_path = bld.env['LIBDIR']
    )

    headers = [
        'include/fountain/fountain.h',
        'include/fountain/error.h',
        'include/fountain/log.h',
        'include/fountain/parse.h',
        'include/fountain/data/data-config.h',
        'include/fountain/data/screenplay.h',
        'include/fountain/data/section.h',
        'include/fountain/parser/parser.h',
        'include/fountain/parser/fountain/parser.h'
    ]
    bld.install_files(bld.env['SLOTINCLUDEDIR'], headers)

    bld(
        features     = 'subst',
        source       = 'libfountain.pc.in',
        target       = 'libfountain-%s.pc' % bld.env['SLOT'],
        install_path = bld.env['PKGCONFIGDIR']
    )

    bld(
        features     = 'c cprogram',
        target       = 'test',
        source       = 'src/test.c',
        use          = ['LIBFOUNTAIN'],
        install_path = None
    )
