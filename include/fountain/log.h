/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_LOG_H_
# define _FOUNTAIN_LOG_H_

# include <stdarg.h>

/**
 * Log level enum.
 */
enum fountain_log_level_e
{
    FOUNTAIN_LOG_LEVEL_ERROR,
    FOUNTAIN_LOG_LEVEL_WARNING,
    FOUNTAIN_LOG_LEVEL_INFO,
    FOUNTAIN_LOG_LEVEL_DEBUG
};

/**
 * Log level type.
 */
typedef enum fountain_log_level_e fountain_log_level_t;

/**
 * Type for log functions.
 *
 * @param level the log level
 * @param ptr the user defined pointer
 * @param fmt the format
 * @param ap the variable argument list
 */
typedef void fountain_log_func_t(
    fountain_log_level_t level,
    void * ptr,
    const char * fmt,
    va_list ap);

typedef struct fountain_log_s fountain_log_t;

/**
 * Create a new logger.
 *
 * @param func the logging function
 * @param ptr the user pointer to pass to the logging function
 * @return the newly created logger, or NULL if there was not enough
 *         memory
 */
fountain_log_t * fountain_log_new(
    fountain_log_func_t * func,
    void * ptr);

/**
 * Destroy a logger.
 * 
 * @param log the log
 */
void fountain_log_destroy(fountain_log_t * log);

#endif /* _FOUNTAIN_LOG_H_ */
