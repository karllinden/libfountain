/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_PARSER_PARSER_H_
# define _FOUNTAIN_PARSER_PARSER_H_

# include <stdio.h>

# include <fountain/error.h>
# include <fountain/data/screenplay.h>

/**
 * Structure for parsers.
 */
struct fountain_parser_info_s
{
    /**
     * The parse function of the parser.
     *
     * The parser must parse the given file respecting the data
     * configuration and filling in error if an error occurs.
     *
     * @param file the file to parse
     * @param config the data configuration
     * @param error the error
     * @return the parsed screenplay, or NULL if an error occured, in
     *         which case error is filled in
     */
    fountain_screenplay_t * (*parse)(
        FILE * file,
        const fountain_data_config_t * config,
        fountain_error_t * error);
};
typedef struct fountain_parser_info_s fountain_parser_info_t;

#endif /* _FOUNTAIN_PARSER_PARSER_H_ */
