/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_DATA_TYPES_H_
# define _FOUNTAIN_DATA_TYPES_H_

# include <stddef.h>

typedef struct fountain_data_config_s fountain_data_config_t;
typedef struct fountain_screenplay_s fountain_screenplay_t;
typedef struct fountain_section_s fountain_section_t;
typedef struct fountain_text_s fountain_text_t;

#endif /* _FOUNTAIN_DATA_TYPES_H_ */
