/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_DATA_TEXT_H_
# define _FOUNTAIN_DATA_TEXT_H_

# include <fountain/error.h>
# include <fountain/data/types.h>

/**
 * Parse a fountain text object from raw fountain markup.
 *
 * When the text object is no longer used it must be destroyed with
 * fountain_text_destroy.
 *
 * @param raw the raw fountain
 * @param config the data config
 * @param error the error to fill in in case an error occurs
 * @return the parsed text object
 */
fountain_text_t * fountain_text_from_raw(
        const char * raw,
        const fountain_data_config_t * config,
        fountain_error_t * error);

/**
 * Destroy a fountain text.
 *
 * @param text the text to destroy
 */
void fountain_text_destroy(fountain_text_t * text);

#endif /* _FOUNTAIN_DATA_TEXT_H_ */
