/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_DATA_SECTION_H_
# define _FOUNTAIN_DATA_SECTION_H_

# include <fountain/error.h>
# include <fountain/data/types.h>

/**
 * Get the data of a section.
 *
 * @see fountain_screenplay_new_with_data_size
 * @return the data
 */
void * fountain_section_data(fountain_section_t * section);

/**
 * Get the last subsection of a section.
 *
 * @param section the section
 * @return the last subsection
 */
fountain_section_t * fountain_section_last(
    const fountain_section_t * section);

/**
 * Create and append a section to a section.
 *
 * This adds the newly created section as a subsection of the given
 * section.
 *
 * @param section the section
 * @param error the error structure
 * @return the newly created subsection
 */
fountain_section_t * fountain_section_append_section(
    fountain_section_t * section,
    fountain_error_t * error);

#endif /* _FOUNTAIN_DATA_SECTION_H_ */
