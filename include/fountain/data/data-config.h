/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_DATA_DATA_CONFIG_H_
# define _FOUNTAIN_DATA_DATA_CONFIG_H_

# include <stddef.h>

# include <fountain/data/screenplay.h>
# include <fountain/data/section.h>

/**
 * Structure containing size's of the extra data of the fountain
 * objects.
 */
struct fountain_data_config_s
{
    /**
     * Pointer to pass to all supplied functions.
     */
    void * pointer;

    /**
     * Size of the extra data of the screenplay objects.
     */
    size_t screenplay_size;

    /**
     * Function to call when initializing a screenplay.
     */
    void (*screenplay_init)(fountain_screenplay_t * , void *);

    /**
     * Function to call when deinitializing a screenplay.
     */
    void (*screenplay_deinit)(fountain_screenplay_t *, void *);

    /**
     * Size of the extra data of the section objects.
     */
    size_t section_size;

    /**
     * Function to call when initializing a section.
     */
    void (*section_init)(fountain_section_t *, void *);

    /**
     * Function to call when deinitializing a section.
     */
    void (*section_deinit)(fountain_section_t *, void *);

    /**
     * Size of the extra data of the text objects.
     */
    size_t text_size;

    /**
     * Function to call when initializing a text object.
     */
    void (*text_init)(fountain_text_t * text, void *);

    /**
     * Function to call when deinitializing a text object.
     */
    void (*text_deinit)(fountain_text_t * text, void *);
};

#endif /* _FOUNTAIN_DATA_DATA_CONFIG_H_ */
