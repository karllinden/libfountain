/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_DATA_SCREENPLAY_H_
# define _FOUNTAIN_DATA_SCREENPLAY_H_

# include <fountain/data/data-config.h>
# include <fountain/data/section.h>
# include <fountain/data/title-page.h>
# include <fountain/data/types.h>

/**
 * Create a new empty screenplay.
 *
 * @param error the error
 * @param config the data configuration, or NULL if no extra data
 *               configuration is necessary
 * @return the newly created screenplay, or NULL if creation failed, in
 *         which case the error is filled in
 */
fountain_screenplay_t * fountain_screenplay_new(
    const fountain_data_config_t * config,
    fountain_error_t * error);

/**
 * Destroy a screenplay freeing any resource held by it.
 *
 * This function destroys all of the screenplay content.
 *
 * @param screenplay the screenplay to destroy
 */
void fountain_screenplay_destroy(fountain_screenplay_t * screenplay);

/**
 * Get the data of a screenplay.
 *
 * This function is only meaningful if the screenplay was created with
 * fountain_screenplay_new_with_data_size, or otherwise there is no
 * extra allocated data.
 *
 * @return the data
 */
void * fountain_screenplay_data(fountain_screenplay_t * screenplay);

/**
 * Get the title page of the screenplay.
 *
 * @return the title page of the screenplay
 */
fountain_title_page_t * fountain_screenplay_title_page(
        fountain_screenplay_t * screenplay);

/**
 * Get the last section of a screenplay.
 *
 * @param screenplay the screenplay
 * @return the last section, or NULL if there are no sections
 */
 fountain_section_t * fountain_screenplay_last_section(
    const fountain_screenplay_t * screenplay);

/**
 * Append a section to a screenplay.
 *
 * @param screenplay the screenplay
 * @return the newly created section, or NULL if an error occured, in
 *         which case error is filled in properly
 */
fountain_section_t * fountain_screenplay_append_section(
    fountain_screenplay_t * screenplay,
    fountain_error_t * error);

#endif /* _FOUNTAIN_DATA_SCREENPLAY_H_ */
