/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_DATA_TITLE_PAGE_H_
# define _FOUNTAIN_DATA_TITLE_PAGE_H_

# include <fountain/error.h>
# include <fountain/data/text.h>

typedef struct fountain_title_page_s fountain_title_page_t;

/**
 * Add an entry to the title page.
 *
 * The screenplay takes ownership of the value, so the value must not be
 * freed by fountain_text_destroy or inserted into any other fountain
 * element.
 *
 * If the key already exists the value of the previously stored entry is
 * overriden.
 *
 * @param screenplay the screenplay
 * @param key the key to add
 * @param value the value
 * @param error the error
 * @return zero on success, or non-zero otherwise,  in which case error
 *         is is filled in accordingly
 */
int fountain_title_page_add_entry(
        fountain_title_page_t * title_page,
        const char * key,
        fountain_text_t * value,
        fountain_error_t * error);

#endif /* _FOUNTAIN_DATA_TITLE_PAGE_H_ */
