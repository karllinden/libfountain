/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef _FOUNTAIN_ERROR_H_
# define _FOUNTAIN_ERROR_H_

# include <stdio.h>

/**
 * Enum of possible library errors.
 */
enum fountain_err_e
{
    /**
     * No error.
     */
    FOUNTAIN_ERROR_NONE,

    /**
     * An error occured when reading a file.
     */
    FOUNTAIN_ERROR_FREAD,

    /**
     * An error occured in a standard library or system call.
     */
    FOUNTAIN_ERROR_SYSTEM
};

/**
 * Type for error codes.
 */
typedef enum fountain_err_e fountain_err_t;

/**
 * Struct carrying detailed error data.
 */
struct fountain_error_s
{
    /**
     * Error code.
     */
    fountain_err_t err;

    /**
     * Error data union carrying error specifc details.
     */
    union {
        /**
         * Error data for FOUNTAIN_ERROR_FREAD.
         */
        struct {
            /**
             * The file that could not be read.
             */
            FILE * file;
        } fread;

        /**
         * Error data for FOUNTAIN_ERROR_SYSTEM.
         */
        struct {
            /**
             * Value of errno, or zero if the call that failed does not
             * use errno.
             */
            int err;

            /**
             * Name of the function that failed.
             */
            const char * call;
        } system;
    } data;
};

/**
 * Type for the error structure.
 */
typedef struct fountain_error_s fountain_error_t;

/**
 * Initialize an error structure.
 *
 * This initializes the error structure with FOUNTAIN_ERROR_NONE and
 * undefined contents of the data union.
 *
 * @param error the error
 */
void fountain_error_init(fountain_error_t * error);

#endif /* _FOUNTAIN_ERROR_H_ */
