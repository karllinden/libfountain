/*
 * This file is part of libfountain.
 *
 * Copyright (C) 2018 Karl Linden <karl.j.linden@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdio.h>

#include <fountain/error.h>
#include <fountain/data/screenplay.h>
#include <fountain/parse.h>

int
main(void)
{
    FILE * file = fopen("example.fountain", "r");
    if (file == NULL) {
        perror("fopen");
        return 1;
    }

    fountain_error_t error;
    fountain_error_init(&error);

    fountain_screenplay_t * screenplay;
    screenplay = fountain_parse_fountain(file, NULL, &error);
    fountain_screenplay_destroy(screenplay);

    fclose(file);
    return 0;
}
